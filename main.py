#!/usr/bin/python
# Script that updates my Xaiomi 1s Desk light that is running custom esp32 firmware: Tasmota
# change light_ip in utility.py    TODO: MOVE TO EXTERNAL FILE, OR ANYWHERE THAT MAKES MORE SENSE
# TODO: maybe run script on login

from datetime import *
from crontab import check_cron
from util import *


# get status of nightlight settings using gsettings
gsettings = get_gsettings()
# convert to tuple from hh.min/60 string
a_time = normalise_time(gsettings[0])
b_time = normalise_time(gsettings[1])
enabled = gsettings[2] == "true"

# Check cron to see if schedule matches nightlight times
check_cron(a_time , b_time) # TODO: change it so cron doesn't have to be checked/updated each time nightlight changes. Increase cron increments

if (enabled):
    temp = int(gsettings[3][7:10])  # extract temp from string output. Remove last digit 4700 > 470
    temp = int(500 - ((temp - 170) / 3))  # offset temp for lamp
    # turn 30,23 to 3023 for comparison
    flat_current_time = flatten(datetime.now().strftime("%H"),datetime.now().strftime("%M"))
    flat_to_time = flatten(b_time[0],b_time[1])
    flat_from_time = flatten(a_time[0],a_time[1])
    # if time in range, change to night light colour temp
    if (flat_from_time < flat_to_time) and (flat_to_time < flat_current_time > flat_from_time):
        setTemp(250)
    elif (flat_from_time > flat_current_time > flat_to_time): 
        setTemp(250)
    else:
        setTemp(temp)
else:
    setTemp(250)
