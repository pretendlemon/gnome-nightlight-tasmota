from util import run_command
import os
import sys

# configures crontab so that script is run when nighlight is scheduled to change

path = os.path.dirname(os.path.abspath(sys.argv[0]))

def cron_exists_all():
    return run_command(f"""crontab -l | grep "\* \* \* " | grep "{path}/main.py" """) != ""

def cron_exists(hour, minute):
    return run_command(f"""crontab -l | grep "{minute} {hour} \* \* \* " | grep "{path}/main.py" """) != ""

def get_cron():
    return run_command(f"""crontab -l | grep "\* \* \* " | grep "{path}/main.py" """)

def gen_cron(hour, minute):
    return f"{minute} {hour} * * * '{path}/main.py'"

def add_cron(hour, minute):
    run_command(f"""(crontab -l ; echo "{gen_cron(hour, minute)}") | crontab -""")

def remove_cron_all():
    run_command(f"""crontab -l | grep -v "\* \* \* " | grep "{path}/main.py" | crontab -""")

def cron_to_time(cron):
    cron = cron.split(" ")
    return int(cron[1]), int(cron[0])

def regen_cron(time_a, time_b):
    remove_cron_all()
    add_cron(time_a[0], time_a[1])
    add_cron(time_b[0], time_b[1])

# check to see if cron jobs present
# update if changed
def check_cron(time_a, time_b):
    if cron_exists_all():
        cronjobs = get_cron().split("\n")[0:-1]  # remove last entries new line
        if len(cronjobs) != 2:
            regen_cron(time_a, time_b)
        else:
            a_time = cron_to_time(cronjobs[0])
            b_time = cron_to_time(cronjobs[1])
            if (a_time != time_a) or (b_time != time_b):
                regen_cron(time_a, time_b)