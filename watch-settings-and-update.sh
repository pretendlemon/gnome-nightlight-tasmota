#!/bin/bash
# A quick and dirty way of watching for changes to night light settings. Not the best. And running the main script requires cron jobs to be checked again
# not reliable

state=$(sh ./get_gsettings.sh)

while true; do
	statetwo=$(sh ./get_gsettings.sh)
	if [ "$statetwo" != "$state" ]; then
		./main.py
	fi
	state=$(sh ./get_gsettings.sh)
	sleep 0.25
done
