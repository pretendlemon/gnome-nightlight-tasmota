import os
import subprocess
import requests

# The ip address of your Tasmota Light
light_ip="192.168.1.213"

# run linux commands. Used for get_gsettings.sh and for configuring crontab
def run_command(command):
    temp_file = open("temp.txt", 'w')
    subprocess.call(command, shell=True, stdout=temp_file)
    with open("temp.txt", 'r') as file:
        output = file.read()
        os.remove("temp.txt")
    return output


def get_gsettings():
    return run_command('bash ./get_gsettings.sh').split(":")

# convert the time format used by gsettings into something more sensible
def normalise_time(t_time):
    hour = ""
    minute = "0."
    dotfound = False
    for i in range(0, len(t_time)):
        char = t_time[i]
        if dotfound:
            minute += char
        else:
            if char == ".":
                dotfound = True
            else:
                hour += char
    minute = int(60 * float(minute))
    t_time = (int(hour), minute)
    return t_time

def flatten(hour, minute):
    if len(str(minute)) == 1:
        minute = f"0{minute}"
    return int(str(f"{hour}{minute}"))

# send request to light with color temp
def setTemp(temp):
    requests.get(f"http://{light_ip}/?m=1&t0={temp}")