#!/bin/bash

from=$(gsettings get org.gnome.settings-daemon.plugins.color night-light-schedule-from)
to=$(gsettings get org.gnome.settings-daemon.plugins.color night-light-schedule-to)
enabled=$(gsettings get org.gnome.settings-daemon.plugins.color night-light-enabled)
temp=$(gsettings get org.gnome.settings-daemon.plugins.color night-light-temperature)

echo "$from:$to:$enabled:$temp"