# gnome-nightlight-tasmota

main.py

This small python script:
1. reads nightlight settings from gnome using gsettings
2. configures/updates crontab so that the script is run when the nightlight is scheduled to turn on and off
3. connects to a light with tasmota and sets the color temperature to be somewhat aligned to the nightlight's temperature

I made this for my Mi Light 1S Desk Lamp running tasmota. I flashed tasmota following the instructions [here](https://github.com/xoste49/MJTD01SYL)
